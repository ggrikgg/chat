#include "stdafx.h"
#include <winsock2.h>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;
const int MessSize = 512;
char message[MessSize];
const int nameSize = 32;
wchar_t name[nameSize];

DWORD WINAPI Reading(LPVOID pinfo);
DWORD WINAPI Writing(LPVOID pinfo);

int main(int argc, char* argv[])
{
	setlocale (LC_ALL, "Russian_Russia.866");

	WSADATA ws;
	WSAStartup (MAKEWORD(2,0),&ws);

	SOCKET clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	 if (clientSocket == INVALID_SOCKET)
	 {
        std::cout<<"������"<< WSAGetLastError()<<std::endl;
        WSACleanup();
	//	system ("Pause");
        exit(-1);
    }

    size_t convertedChars = 0;
    mbstowcs_s(&convertedChars, name, nameSize, argv[3], _TRUNCATE);

	sockaddr_in saddr;
	memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(atoi(argv[2]));
	cout << argv[1] << endl;
    saddr.sin_addr.s_addr = inet_addr(argv[1]);

	if(connect(clientSocket, (sockaddr*) &saddr, sizeof(saddr)) ==	SOCKET_ERROR) {
		std::cout<<"connect failed with error: "<< WSAGetLastError()<<std::endl;
		shutdown(clientSocket,0);
        closesocket(clientSocket);
        WSACleanup();
	//	system ("Pause");
      //  exit(-1);
    }

	HANDLE handles[2];
	handles[0] = CreateThread(NULL, 0, Reading, (LPVOID)&clientSocket, 0, NULL);
	handles[1] = CreateThread(NULL, 0, Writing, (LPVOID)&clientSocket, 0, NULL);

	WaitForMultipleObjects(2, handles, FALSE, INFINITE);

	CloseHandle(handles[0]);
	CloseHandle(handles[1]);
	
	shutdown(clientSocket,0);
	closesocket(clientSocket);

	WSACleanup();
//	system ("Pause");
	return 0;
}

DWORD WINAPI Reading(LPVOID pinfo) {
	SOCKET sock = *(SOCKET*)pinfo;
	wchar_t name[nameSize];
	wchar_t Mess[MessSize];
	wmemset(Mess, 0, MessSize);

	int i = 0;
	while (true) {
		wchar_t Mess[MessSize];
		wmemset(Mess, 0, MessSize);
		if (recv(sock, (char*)Mess, MessSize* sizeof(wchar_t), 0) == -1)
			ExitThread(1);
		std::wcout << Mess << std::endl;
//		wprintf(L"%s\n",  Mess);
		wmemset(Mess, 0, MessSize);
		if (wcscmp(Mess, L"�����") == 0)
			ExitThread(1);
	}
}

DWORD WINAPI Writing(LPVOID pinfo) {
	SOCKET sock = *(SOCKET*)pinfo;
	send(sock, (char*)name, nameSize*sizeof(wchar_t), 0);

	while(true) {
		wchar_t Mess[MessSize];
		wmemset(Mess, 0, MessSize);
		wcin.getline(Mess,506);
		wcin.clear();
		send(sock, (char*)Mess, wcslen(Mess)*sizeof(wchar_t), 0);
		if (wcscmp(Mess, L"�����") == 0)
		ExitThread(1);
	}
}
