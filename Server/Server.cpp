#include "stdafx.h"
#include <winsock2.h>
#include <iostream>
#include <string>
#include <cstring>
#include <map>

using namespace std;

const int MessSize = 512;
map<SOCKET, wchar_t*> clients;

const int ListUserSize = 28;
const int nameSize = 32;

const int HelloMessageSize = 32;
wchar_t HelloMessage[HelloMessageSize];
 

DWORD WINAPI Reading(LPVOID pinfo);
DWORD WINAPI Writing(LPVOID pinfo);
void errorMessage(SOCKET serverSocket);
void errorMessageThread(SOCKET serverSocket);

int main(int argc, char* argv[])
{

	setlocale (LC_ALL, "Russian");

	WSADATA ws;
	WSAStartup (MAKEWORD(2,0),&ws);

	SOCKET serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	 if (serverSocket == INVALID_SOCKET)
	 {
		wprintf(L"�������� ������ ����������� ������� � �������: %ld\n", WSAGetLastError());
        WSACleanup();
		system ("Pause");
        exit(-1);
    }

	sockaddr_in saddr;
	memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
	saddr.sin_port = htons(atoi(argv[1]));
	//saddr.sin_port = htons(atoi(argv[2]));
	saddr.sin_addr.s_addr =INADDR_ANY;
   // saddr.sin_addr.s_addr = inet_addr(argv[1]);

	if (bind(serverSocket, (sockaddr*) &saddr, sizeof(saddr)) == SOCKET_ERROR)
	{
        wprintf(L"���������� (bind) ����������� ������� � �������: %ld\n", WSAGetLastError());
		errorMessage(serverSocket);
    }

    if (listen(serverSocket, 1) == SOCKET_ERROR)
	{
        wprintf(L"��������� (listen) ����������� ������� � �������: %ld\n", WSAGetLastError());
		errorMessage(serverSocket);
    }
	int Res = 1;
    wprintf(L"�������� �������� ��� ������ ������...\n");

	HANDLE *handles = new HANDLE[10];
	int i = 0;

	while (true) {
		int len = sizeof(saddr);
		SOCKET clientSocket = accept(serverSocket, (sockaddr*)&saddr, &len);	  
		if (clientSocket == INVALID_SOCKET)
		{
			wprintf(L"����� (accept) ���������� ������� � �������: %ld\n", WSAGetLastError());
			errorMessage(clientSocket);
		}

		clients[clientSocket] = NULL;
		handles[i++] = CreateThread(NULL, 0, Reading, (LPVOID)&clientSocket, 0, NULL);


	}
	CloseHandle(handles[i-1]);
	
	closesocket(serverSocket);

	system("Pause");
	return 0;
}

DWORD WINAPI Reading(LPVOID pinfo) {
	SOCKET sock = *(SOCKET*)pinfo;
	wchar_t name[nameSize];
	wchar_t Mess[MessSize];
	wmemset(Mess, 0, MessSize);

	if (recv(sock, (char*)name, nameSize* sizeof(wchar_t), 0) == -1)
			ExitThread(1);
	clients[sock] = (wchar_t*)name;

	wcscpy(HelloMessage, L"����������� ������������: ");
	wcscat(HelloMessage, name);

	for (std::map<SOCKET, wchar_t*>::iterator it=clients.begin(); it!=clients.end(); ++it) {
			if (it->first != sock) 	
				send(it->first, (char*)HelloMessage, HelloMessageSize*sizeof(wchar_t), 0);
	}

	
	wchar_t ListUser[ListUserSize];
	wcscpy(ListUser, L"������ ��������� ����:\n");
	
	for (std::map<SOCKET, wchar_t*>::iterator it=clients.begin(); it!=clients.end(); ++it) {
		wcscat(ListUser, it->second);
		wcscat(ListUser, L"\n");
	}
	send(sock, (char*)ListUser, MessSize * sizeof(wchar_t), 0);
	wchar_t firstChar[2];
	firstChar[1] = '\0';
	wchar_t* it;
	const int nameSecondClientSize = 32;
	wchar_t nameSecondClient[nameSecondClientSize];
	wmemset(nameSecondClient, 0, nameSecondClientSize);
	int i = 0;
	
	wchar_t privateMessage[MessSize];

	while (true) {
		wmemset(Mess, 0, MessSize);
		wmemset(privateMessage, 0, MessSize);
		if (recv(sock, (char*)Mess, MessSize* sizeof(wchar_t), 0) == -1)
			ExitThread(1);
		firstChar[0] = Mess[0];
		if (wcscmp(firstChar, L"#") == 0) {
			it = wcschr(Mess, ':');
			it--;
			for (wchar_t* itFirst = Mess; itFirst < it; itFirst++) {
				++i;
				nameSecondClient[i-1] = Mess[i];
			}

			it = wcschr(Mess, ':');

			wcscat(privateMessage, clients[sock]);
			wcscat(privateMessage, L" ����� ���: ");
			wcscat(privateMessage, it+2);


			for (std::map<SOCKET, wchar_t*>::iterator it=clients.begin(); it!=clients.end(); ++it) {
				if (wcscmp(it->second, nameSecondClient) == 0) {
					send(it->first, (char*)privateMessage, MessSize*sizeof(wchar_t), 0);
				}
			}
			//wprintf(L"%s > %s: %s\n", clients[sock], nameSecondClient, Messs+(i+2));
			i = 0;
			wmemset(privateMessage, 0, MessSize);
		}
		else {
			wchar_t publicMessage[MessSize];
			wmemset(publicMessage, 0, MessSize);
			wcscat(publicMessage, clients[sock]);
			wcscat(publicMessage, L": ");
			wcscat(publicMessage, Mess);
			for (std::map<SOCKET, wchar_t*>::iterator it=clients.begin(); it!=clients.end(); ++it) {
				if (it->first != sock) 	
					send(it->first, (char*)publicMessage, MessSize*sizeof(wchar_t), 0);
			}
		}
		if (wcscmp(Mess, L"�����") == 0) {
			wmemset(Mess, 0, MessSize);
			wcscat(Mess, L"��������� ���!");
			send(sock, (char*)Mess, MessSize*sizeof(wchar_t), 0);
			ExitThread(-2);
		}
	}
}


void errorMessage(SOCKET serverSocket) {

	closesocket(serverSocket);
	WSACleanup();
	system("Pause");
	exit(-1);
}

void errorMessageThread(SOCKET serverSocket) {

	closesocket(serverSocket);
	WSACleanup();
	system("Pause");
	ExitThread(-1);
}